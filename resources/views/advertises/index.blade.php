@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <tr>
                            <th>State</th>
                            <th>Text</th>
                            <th>Image</th>
                            <th>Video</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($advertises as $advertise)
                            <tr>
                                <td>{{ $advertise->state }}</td>
                                <td>{{ $advertise->text }}</td>
                                <td>{{ $advertise->image }}</td>
                                <td>{{ $advertise->video }}</td>
                                <td>
                                    <form action="{{ route('advertise.destroy',$advertise->id) }}" method="POST">

                                        <a class="btn btn-info"
                                           href="{{ route('advertise.show',$advertise->id) }}">Show</a>

                                        <a class="btn btn-primary" href="{{ route('advertise.edit',$advertise->id) }}">Edit</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    {!! $advertises->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection