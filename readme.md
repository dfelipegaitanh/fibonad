# Prueba Diego Gaitán
I developed this test in [Laravel 6](https://laravel.com/docs/6.x "Laravel 6") due I don't have the knowledge in creating applications in [Symfony](https://symfony.com/ "Symfony"). I also choose Laravel for develop this test because it's core is based on Symfony

## How To install
After you clone the repository go to a shell console and execute
> composer install

Create a MySql database call *fibonad*
>  mysql -u-p > create database fibonad

### Database

Configure your database connection in the *.env* file
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=fibonad
DB_USERNAME=root
DB_PASSWORD=
```

#### Database migration and Database seed

Open a shell console y execute this commands

- Database migration
> php artisan migrate

- Database seed
> php artisan db:seed


------------

You can log in to the application with the user *fibonad@fibonad.com* and the password *password*