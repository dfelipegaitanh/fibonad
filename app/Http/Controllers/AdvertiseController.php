<?php

namespace App\Http\Controllers;

use App\Advertise;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdvertiseController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $advertises = Advertise::latest()->paginate(5);

        return view("advertises.index", compact("advertises"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Advertise $advertise
     * @return Response
     */
    public function show(Advertise $advertise) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Advertise $advertise
     * @return Response
     */
    public function edit(Advertise $advertise) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request   $request
     * @param Advertise $advertise
     * @return Response
     */
    public function update(Request $request, Advertise $advertise) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Advertise $advertise
     * @return Response
     */
    public function destroy(Advertise $advertise) {
        //
    }
}
