<?php

/** @var Factory $factory */

use App\Advertise;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Advertise::class, function (Faker $faker) {
    $states = ['published', 'stopped', 'publishing'];
    return [
            'state' => $states[rand(0, 2)],
            'image' => $faker->imageUrl(),
            'video' => $faker->imageUrl(),
            'text'  => $faker->text(140),
    ];

});
